package db;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import com.alibaba.fastjson.JSON;

public class DB {
	
	public static String dbFilePath=Thread.currentThread().getContextClassLoader()
			.getResource("").getPath()
			+ "db/db.json";
	public static Map<String, Object> dbMap = null;
	public static Map<String, List<Map<String, Object>>> tableMap = null;
	public static Map<String, Object> recordMap = null;
	
	public static void main(String[] args) throws Exception {
		
		//选择,读取数据库
		useDB("db5");
		//插入数据 
		insert("table1","{name:'name1',age:10,class:[{name:'class1'},{name:'class2'}]}");
		insert("table2","{name:'name1',age:11,class:[{name:'class1'},{name:'class2'}]}");
		insert("table2","{name:'name1',age:12,class:[{name:'class3'},{name:'class3'}]}");
		insert("table2","{name:'name2',age:13,class:[{name:'class3'},{name:'class4'}]}");
		//修改数据
		update("table2","name","name1","age","10");
		//查询数据
		find("table2","name","name1");
		//删除数据
		delete("table2","name","name1");
		//查询数据
		find("table2","name","name1");
		//删除表
		drop("table2");
		//查询数据
		find("table2","name","name1");
		//所有数据
		System.out.println("输出所有数据");
		String jsonString = JSON.toJSONString(dbMap);
		System.out.println(jsonString);
		//保存数据
		submit();

	}
	
	
	public static void submit() throws Exception{
		String jsonString = JSON.toJSONString(dbMap);
		File dbFile = new File(dbFilePath);
		writeTxtFile(jsonString,dbFile);
	}
	
	
	//删除表
	public static void drop(String tableName){
		boolean del=false;
		List<Map<String, Object>> recordsList=tableMap.get(tableName);
		if(recordsList==null){
			System.out.println("删除失败,表不存在"+tableName);
			return;
		}else{
			Iterator<Entry<String, List<Map<String, Object>>>> it = tableMap.entrySet().iterator();  
	        while(it.hasNext()){  
	            Entry<String, List<Map<String, Object>>> entry=it.next();  
	            String key=entry.getKey();  
	            if(tableName.equals(key)){  
	                it.remove();
	                System.out.println("删除表成功"+tableName);
	            }  
	        } 
		}
	}
	
	//删除数据
	public static void delete(String tableName,String fieldName,String value){
		boolean del=false;
		List<Map<String, Object>> recordsList=tableMap.get(tableName);
		if(recordsList==null){
			System.out.println("删除失败,表不存在"+tableName);
			return;
		}
		for(int i=recordsList.size()-1;i>=0;i--){
			recordsList.remove(i);
			del=true;
		}
		if(del){
			System.out.println("删除成功");
		}else{
			System.out.println("删除失败,记录不存在");
		}
	}
	
	//查询数据
	public static void find(String tableName,String fieldName,String value){
		boolean notFound=true;
		List<Map<String, Object>> recordsList=tableMap.get(tableName);
		if(recordsList==null){
			System.out.println("查询失败,表不存在"+tableName);
			return;
		}
		for(Map<String, Object> record:recordsList){
			if(value.equals(record.get(fieldName))){
				System.out.println(JSON.toJSONString(record));
			}
			notFound=false;
		}
		if(notFound){
			System.out.println("查询记录不存在");
		}
	}
	
	//修改数据
	public static void update(String tableName,String fieldName,String value,String setField,String setValue){

		List<Map<String, Object>> recordsList=tableMap.get(tableName);
		if(recordsList==null){
			System.out.println("修改失败,表不存在"+tableName);
			return;
		}
		for(Map<String, Object> record:recordsList){
			if(value.equals(record.get(fieldName))){
				record.put(setField, setValue);
			}
		}
	}
	
	//插入数据 Map,表不存在就新建
	public static void insert(String tableName,Map recordMap){

		List recordsList=tableMap.get(tableName);
		if(recordsList==null){
			recordsList=new ArrayList();
			tableMap.put(tableName, recordsList);
		}
		if(recordMap.get("id")==null){
			recordMap.put("id", UUID.randomUUID());
		}
		//String recordString = JSON.toJSONString(recordMap);
		recordsList.add(recordMap);
	}
	//插入数据 String
	public static void insert(String tableName,String record){
		Map recordMap=JSON.parseObject(record, Map.class);
		insert(tableName,recordMap);
	}
	
	//使用db    db不存在就新建
	public static void useDB(String name) throws Exception{
		File dbFile = new File(dbFilePath);
		createFile(dbFile);
		String dbJson = readTxtFile(dbFile);
		dbMap = (Map<String, Object>) JSON.parseObject(dbJson, Map.class);
		tableMap=(Map<String,List<Map<String, Object>>>)dbMap.get(name);
		if(tableMap==null){
			tableMap=new HashMap<String, List<Map<String, Object>>>();
			dbMap.put(name, tableMap);
		}
	}
	
	/**
	 * 创建文件
	 * 
	 * @param fileName
	 * @return
	 */
	public static boolean createFile(File fileName){
		boolean flag = false;
		try {
			if (!fileName.exists()) {
				fileName.createNewFile();
				flag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	/**
	 * 读TXT文件内容
	 * 
	 * @param fileName
	 * @return
	 */
	public static String readTxtFile(File fileName) throws Exception {
		FileReader fileReader = null;
		BufferedReader bufferedReader = null;
		StringBuffer sb = new StringBuffer();
		try {
			fileReader = new FileReader(fileName);
			bufferedReader = new BufferedReader(fileReader);
			try {
				String read = null;
				while ((read = bufferedReader.readLine()) != null) {
					sb.append(read);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bufferedReader != null) {
				bufferedReader.close();
			}
			if (fileReader != null) {
				fileReader.close();
			}
		}
		return sb.toString();
	}

	public static boolean writeTxtFile(String content, File fileName)
			throws Exception {
		RandomAccessFile mm = null;
		boolean flag = false;
		FileOutputStream o = null;
		try {
			o = new FileOutputStream(fileName);
			o.write(content.getBytes("GBK"));
			o.close();
			// mm=new RandomAccessFile(fileName,"rw");
			// mm.writeBytes(content);
			flag = true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (mm != null) {
				mm.close();
			}
		}
		return flag;
	}
}
